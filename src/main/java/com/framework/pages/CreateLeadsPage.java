package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadsPage extends ProjectMethods{

	public CreateLeadsPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompName;
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.NAME,using="submitButton") WebElement eleCreateLeadBtn;
	public CreateLeadsPage enterCompName(String data) {
		clearAndType(eleCompName, data);
		return this;
	}
	
	public CreateLeadsPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}
	
	public CreateLeadsPage enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}
	
	public ViewLeadPage clickCreateLead1() {
		click(eleCreateLeadBtn);
		return new ViewLeadPage();
	}
	
	


}
















