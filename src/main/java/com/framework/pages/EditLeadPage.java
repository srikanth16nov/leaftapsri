package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID,using="updateLeadForm_companyName") WebElement eleEditCompName;
	@FindBy(how=How.XPATH,using="//input[@value='Update']") WebElement eleUpdateButton;

	
	public EditLeadPage editCompName(String data) {
		clearAndType(eleEditCompName, data);
		return this;
	}
			
	public ViewLeadPage clickUpdateButton() {
		click(eleUpdateButton);
		return new ViewLeadPage();
	}


}
















