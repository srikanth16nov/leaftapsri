package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadPage extends ProjectMethods{

	public FindLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH,using="(//input[@name='firstName'])[3]") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']") WebElement eleFLButton;
	@FindBy(how=How.XPATH,using="(//a[@class='linktext'])[4]") WebElement eleFirstId;
	
	public FindLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}
		
	public FindLeadPage clickFindLead() {
		click(eleFLButton);
		return this;
	}
	
	public ViewLeadPage clickFirstLeadId() {
		click(eleFirstId);
		return new ViewLeadPage();
	}


}
















