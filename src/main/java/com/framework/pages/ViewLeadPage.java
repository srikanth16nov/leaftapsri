package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="viewLead_firstName_sp") WebElement eleFirstName;
	@FindBy(how=How.LINK_TEXT,using="Edit") WebElement eleEditButton;

	public void verifyFirstname(String data) {
		verifyExactText(eleFirstName, data);
		
	}
	
	public EditLeadPage clickEditButton() {
		click(eleEditButton);
		return new EditLeadPage();
	}
	


}
















