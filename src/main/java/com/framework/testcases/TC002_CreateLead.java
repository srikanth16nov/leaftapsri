package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_CreateLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="Create Lead";
		testNodes="Lead";
		author="Srikanth";
		category="sanity";
		dataSheetName="TC002";

	}
	@Test(dataProvider="fetchData")
	public void login(String userName,String password,String compName,String firstName,String lastName) {
		new LoginPage()
		.enterUsername(userName) 
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompName(compName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLead1()
		.verifyFirstname(firstName);

			
	}
	
}
