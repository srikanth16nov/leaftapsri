package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC003_EditLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC003_EditLead";
		testDescription="Edit Lead";
		testNodes="Lead";
		author="Srikanth";
		category="sanity";
		dataSheetName="TC003";

	}
	@Test(dataProvider="fetchData")
	public void login(String userName,String password,String firstName,String compName) {
		new LoginPage()
		.enterUsername(userName) 
		.enterPassword(password)
		.clickLogin()
		.clickCrmSfa()
		.clickLeads()
		.clickFindLead()
		.enterFirstName(firstName)
		.clickFindLead()
		.clickFirstLeadId()
		.clickEditButton()
		.editCompName(compName)
		.clickUpdateButton();

			
	}
	
}
